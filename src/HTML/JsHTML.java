/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HTML;

import java.util.ArrayList;
import javafx.collections.ObservableList;
import ssm.model.Slide;
import ssm.model.SlideShowModel;

/**
 *
 * @author JL
 */
public class JsHTML {
    
    private String jsTopCode;
    private String jsBottomCode;
    private SlideShowModel SlideShow;
    private String slideShowTitle;
    private String firstCap;
    private String firstImage;
    public ArrayList<String> capList = new ArrayList<String>();
    public ArrayList<String> imgList = new ArrayList<String>();;
    private ObservableList<Slide> slideList;
    
    public JsHTML(SlideShowModel SSM){
        SlideShow = SSM;
        slideShowTitle = SSM.getTitle();
        firstCap = SSM.getSlides().get(0).getCaption();
        firstImage = SSM.getSlides().get(0).getImageFileName();
        slideList = SSM.getSlides();
        
        System.out.println("sss" + firstImage);
        setCaptionList();
        setImgList();
        setJsCapList();
        setJsImgList();
        setJsCode();
    }
    
    public void setCaptionList(){
        for (int i =0; i < (slideList.size()); i++) {
            String caption = slideList.get(i).getCaption();
            capList.add(caption);
            System.out.println(caption + "123");
        }
    }
    
    public void setImgList(){
        for (int i =0; i < (slideList.size()); i++) {
            String image = slideList.get(i).getImageFileName();
            imgList.add(image);
        }
    }
    
    public void setJsCapList(){
        jsTopCode = "var capArray = [";
        
        for(int i = 0; i < capList.size(); i++){
            if(i == (capList.size()-1)){
                jsTopCode = jsTopCode + "\"" + capList.get(i) + "\"";
                
            }else{
                jsTopCode = jsTopCode + "\"" + capList.get(i) + "\"" + ",";
            }
        }
        jsTopCode = jsTopCode + "]; \n";
    }
    
    public void setJsImgList(){
        jsTopCode = jsTopCode + "var imgArray = [";
        
        for(int i = 0; i < imgList.size(); i++){
            if(i == (imgList.size()-1)){
                jsTopCode = jsTopCode + "\"Images/" + imgList.get(i) + "\"";
                
            }else{
                jsTopCode = jsTopCode + "\"Images/" + imgList.get(i) + "\""+ ",";
            }
        }
        jsTopCode = jsTopCode + "]; \n";
    }

    
    public void setJsCode(){
        jsBottomCode = "var arrayIndex = 0;\n" +
"var play = false;\n" +
"\n" +
"function setUpSlideShow(){\n" +
"    \n" +
"    var showTitle = document.getElementById(\"slideShowTitle\");\n" +
"    showTitle.innerHTML = \""+ slideShowTitle +"\";\n" +
"    var firstImg = document.getElementById(\"slideImg\");\n" +
"    firstImg.src =\"Images/"+ firstImage +"\";\n" +
"    var firstCap = document.getElementById(\"slideCaption\");\n" +
"    firstCap.innerHTML = \""+ firstCap +"\";\n" +
"}\n" +
"\n" +
"function lastImage(){\n" +
"\n" +
"    console.log(\"MOVE BACK\");\n" +
"    var Image = document.getElementById(\"slideImg\");\n" +
"    var Caption = document.getElementById(\"slideCaption\");\n" +
"    \n" +
"    if(arrayIndex == 0){\n" +
"        Image.src=imgArray[imgArray.length-1];\n" +
"        Caption.innerHTML = capArray[capArray.length-1];\n" +
"        arrayIndex = imgArray.length-1;\n" +
"    }else{\n" +
"        Image.src=imgArray[arrayIndex-1];\n" +
"        Caption.innerHTML = capArray[arrayIndex-1];\n" +
"        arrayIndex = arrayIndex -1;\n" +
"    }\n" +
"}\n" +
"\n" +
"function nextImage(){\n" +
"\n" +
"    console.log(\"Move Foward\");\n" +
"    var Image = document.getElementById(\"slideImg\");\n" +
"    var Caption = document.getElementById(\"slideCaption\");\n" +
"    \n" +
"    if(arrayIndex == (imgArray.length-1)){\n" +
"        Image.src = imgArray[0];\n" +
"        Caption.innerHTML = capArray[0];\n" +
"        arrayIndex = 0;\n" +
"    }else{\n" +
"        Image.src = imgArray[arrayIndex+1];\n" +
"        Caption.innerHTML = capArray[arrayIndex+1];\n" +
"        arrayIndex = arrayIndex + 1\n" +
"    }\n" +
"}\n" +
"\n" +
"var intervalID;\n" +
"\n" +
"function startSlideShow(){\n" +
"    play = !play;\n" +
"    console.log(play);\n" +
"    if(play == true){\n" +
"        var playButt = document.getElementById(\"play\");\n" +
"        playButt.src=\"Images/stop.png\";\n" +
"        playSlideShow();\n" +
"    }else{\n" +
"        var playButt = document.getElementById(\"play\");\n" +
"        playButt.src=\"Images/play.png\";\n" +
"        clearInterval(intervalID);\n" +
"    }\n" +
"\n" +
"}\n" +
"\n" +
"function playSlideShow(){\n" +
"\n" +
"    intervalID = setInterval(function(){nextImage()}, 2000);\n" +
"}";
    }
    
    public String getJsCode(){
        return jsTopCode + jsBottomCode;
    }
}
