/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HTML;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import static ssm.StartupConstants.PATH_ICONS;
import ssm.model.Slide;
import ssm.model.SlideShowModel;

/**
 *
 * @author JL
 */
public class ImgHTML {
    private SlideShowModel SlideShow;
    private String imgDirPath;
    
    
    public ImgHTML(SlideShowModel SSM, String dirPath){
        SlideShow = SSM;
        imgDirPath = dirPath;
        copyImg();
    }
    
    public void copyImg(){
        for (Slide slide : SlideShow.getSlides()) {
            File source = new File(slide.getImagePath() + slide.getImageFileName());
            File dest = new File(imgDirPath +"/" + slide.getImageFileName());
            try {
                if(!dest.exists())
                    Files.copy(source.toPath(), dest.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        
        //SOurce Files
        File playSource = new File(PATH_ICONS + "play.png");
        File stopSource = new File(PATH_ICONS + "stop.png");
        File nextSource = new File(PATH_ICONS + "Next.png");
        File prevSource = new File(PATH_ICONS + "Previous.png");
        
        //Copyed Files
        File play = new File(imgDirPath +"/" + "play.png");
        File stop = new File(imgDirPath +"/" + "stop.png");
        File next = new File(imgDirPath +"/" + "Next.png");
        File prev = new File(imgDirPath +"/" + "Previous.png");
        
        try {
                if(!play.exists())
                    Files.copy(playSource.toPath(), play.toPath());
                if(!stop.exists())
                    Files.copy(stopSource.toPath(), stop.toPath());
                if(!next.exists())
                    Files.copy(nextSource.toPath(), next.toPath());
                if(!prev.exists())
                    Files.copy(prevSource.toPath(), prev.toPath());
                
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

}