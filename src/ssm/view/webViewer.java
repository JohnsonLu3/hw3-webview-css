/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssm.view;


import HTML.ImgHTML;
import HTML.JsHTML;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import static ssm.StartupConstants.PATH_WEBSLIDE;
import ssm.model.Slide;
import ssm.model.SlideShowModel;

/**
 *
 * @author JL
 */
public class webViewer {  

    private SlideShowModel slideShow;
    private ObservableList<Slide> slideShowList;
    private ArrayList captionsToShow;
    private Slide[] listArray;

    public webViewer(SlideShowModel slideModel, ArrayList capList) {

        slideShow = slideModel;
        slideShowList = slideModel.getSlides();
        captionsToShow = capList;

        setArrayList();

        //Creates Main Directory using SlideShow Name
        String dirPath = PATH_WEBSLIDE + slideModel.getTitle();
        File SlideShowDir = new File(dirPath);
        //Creates CSS Directory
        String cssDirPath = SlideShowDir.getAbsolutePath() + "/CSS";
        File cssDir = new File(cssDirPath);
        //Creates JS Directory
        String jsDirPath = SlideShowDir.getAbsolutePath() + "/JS";
        File jsDir = new File(jsDirPath);
        //Creats IMAGE Directory
        String imgDirPath = SlideShowDir.getAbsolutePath() + "/Images";
        File imgDir = new File(imgDirPath);

        if (!SlideShowDir.exists()) {
            SlideShowDir.mkdirs();
        }
        if (!cssDir.exists()) {
            cssDir.mkdir();
        }
        if (!jsDir.exists()) {
            jsDir.mkdir();
        }
        if (!imgDir.exists()) {
            imgDir.mkdir();
        }

        System.out.println(SlideShowDir.toString());

        File indexHTML = new File(SlideShowDir.getAbsolutePath() + "/index.html");
        File baseIndex = new File(PATH_WEBSLIDE + "/basicSlideShow/index.html");

        
        try {
            if(indexHTML.exists()){
                indexHTML.delete();
            }
            Files.copy(baseIndex.toPath(), indexHTML.toPath());

        } catch (IOException e) {
            e.printStackTrace();
        }

        
        String jsPath = jsDir.getAbsolutePath() + "/main.js";
        File jsWEB = new File(jsPath);
        try {
            BufferedWriter writeFile = new BufferedWriter(new FileWriter(jsWEB));
            JsHTML jsCode = new JsHTML(slideShow);
            writeFile.write(jsCode.getJsCode());
            writeFile.close();
        } catch (IOException e) {

        }

        File cssHTML = new File(cssDir.getAbsolutePath() + "/main.css");
        System.out.println(cssDir.getAbsolutePath() + "/main.css");
        File baseCSS = new File(PATH_WEBSLIDE + "/basicSlideShow/CSS/main.css");
        System.out.println(PATH_WEBSLIDE + "/basicSlideShow/CSS/main.css");

        try {
            if(cssHTML.exists()){
                cssHTML.delete();
            }
            Files.copy(baseCSS.toPath(), cssHTML.toPath());

        } catch (IOException e) {
            e.printStackTrace();
        }
        
        
        //Used to copy over the slide images
        ImgHTML imgFiles = new ImgHTML(slideShow, imgDir.getAbsolutePath());

        URL load = null;
        try {
            load = new File(indexHTML.getAbsolutePath()).toURI().toURL();
        } catch (MalformedURLException ex) {
            Logger.getLogger(webViewer.class.getName()).log(Level.SEVERE, null, ex);
        }
        WebView browser = new WebView();
        WebEngine webEngine = browser.getEngine();
        webEngine.load(load.toExternalForm());
        Stage webStage = new Stage();
        Scene scene = new Scene(new Group(), 1280, 720);
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(browser);
        scrollPane.setFitToHeight(true);
        scrollPane.setFitToWidth(true);
        scene.setRoot(scrollPane);

        webStage.setScene(scene);
        webStage.show();

    }

    public void setArrayList() {

        listArray = new Slide[slideShowList.size()];

        for (int i = 0; i < slideShowList.size(); i++) {
            listArray[i] = slideShowList.get(i);
        }
    }

}
