/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssm.view;

import javafx.scene.control.Button;

/**
 *
 * @author JL
 */
public class MoveButtonsView {
    private static Button up = new Button();
    private static Button down = new Button();
    private static Button remove = new Button();
    
    /**
     * Make Move Buttons Passible to SelectedSlide move Enabling
     * and disabling.
     * 
     * @param upButt
     * @param downButt 
     */
    public static void setButtons(Button upButt, Button downButt, Button rm){
        up = upButt;
        down = downButt;
        remove = rm;
    }
    
    public static Button getUpButt(){
        return up;
    }
    
    public static Button getDownButt(){
        return down;
    }
    public static Button getRemoveButt(){
        return remove;
    }
}
