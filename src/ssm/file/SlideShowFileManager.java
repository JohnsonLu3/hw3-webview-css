package ssm.file;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javafx.scene.control.TextInputDialog;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import static ssm.StartupConstants.PATH_SLIDE_SHOWS;
import ssm.model.Slide;
import ssm.model.SlideShowModel;
import ssm.view.captionList;
import javafx.scene.layout.HBox;
import properties_manager.PropertiesManager;
import static ssm.LanguagePropertyType.SAVE;
import static ssm.LanguagePropertyType.SFILE_NAME;
import static ssm.LanguagePropertyType.UNTITLED_MESS;

/**
 * This class uses the JSON standard to read and write slideshow data files.
 * 
 * @author McKilla Gorilla & _____________
 */
public class SlideShowFileManager {
    // JSON FILE READING AND WRITING CONSTANTS
    public static String JSON_TITLE = "title";
    public static String JSON_SLIDES = "slides";
    public static String JSON_CAPTION = "caption";
    public static String JSON_IMAGE_FILE_NAME = "image_file_name";
    public static String JSON_IMAGE_PATH = "image_path";
    public static String JSON_EXT = ".json";
    public static String SLASH = "/";
    public boolean newFile = true;
    private String slideShowTitle = "";
    public static String caption = "caption";
    public static String caption_text;
    /**
     * This method saves all the data associated with a slide show to
     * a JSON file.
     * 
     * @param slideShowToSave The course whose data we are saving.
     * 
     * @throws IOException Thrown when there are issues writing
     * to the JSON file.
     */
    public void saveSlideShow(SlideShowModel slideShowToSave) throws IOException {
        // BUILD THE FILE PATH
        
        if(newFile == false){
            slideShowTitle = "" + slideShowToSave.getTitle();
        }else{
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String SAVE_T = props.getProperty(SAVE.toString());
            String UNTITLED_M = props.getProperty(UNTITLED_MESS.toString());
            String FILENAME_Q = props.getProperty(SFILE_NAME.toString());
            TextInputDialog fileNameInput = new TextInputDialog(UNTITLED_M);
            fileNameInput.setTitle(SAVE_T);
            fileNameInput.setHeaderText(null);
            fileNameInput.setContentText(FILENAME_Q);
            
            Optional<String> result = fileNameInput.showAndWait();

                if (result.isPresent()){
                    slideShowTitle = result.get();
                    slideShowToSave.setTitle(result.get());
                    newFile = false;
                }

        }
        String jsonFilePath = PATH_SLIDE_SHOWS + SLASH + slideShowTitle + JSON_EXT;
        
        // INIT THE WRITER
        OutputStream os = new FileOutputStream(jsonFilePath);
        JsonWriter jsonWriter = Json.createWriter(os);  
           
        // BUILD THE SLIDES ARRAY
        JsonArray slidesJsonArray = makeSlidesJsonArray(slideShowToSave.getSlides());
        // Build The caption array
        JsonArray captionJsonArray = makeCaptionJsonArray(captionList.getList());
        
        // NOW BUILD THE COURSE USING EVERYTHING WE'VE ALREADY MADE
        JsonObject courseJsonObject = Json.createObjectBuilder()
                                    .add(JSON_TITLE, slideShowToSave.getTitle())
                                    .add(JSON_SLIDES, slidesJsonArray)
                                    .add(JSON_CAPTION, captionJsonArray)
                .build();
        
        // AND SAVE EVERYTHING AT ONCE
        jsonWriter.writeObject(courseJsonObject);
    }
    
    public String getTitle(){
        return slideShowTitle;
    }
    public void setnewFile(boolean newF){
        newFile = newF;
    }
    /**
     * This method loads the contents of a JSON file representing a slide show
     * into a SlideSShowModel object.
     * 
     * @param slideShowToLoad The slide show to load
     * @param jsonFilePath The JSON file to load.
     * @throws IOException 
     */
    public void loadSlideShow(SlideShowModel slideShowToLoad, String jsonFilePath) throws IOException {
        // LOAD THE JSON FILE WITH ALL THE DATA
        JsonObject json = loadJSONFile(jsonFilePath);
        
        // NOW LOAD THE COURSE
	slideShowToLoad.reset();
        slideShowToLoad.setTitle(json.getString(JSON_TITLE));
        slideShowTitle = json.getString(JSON_TITLE);
        JsonArray jsonSlidesArray = json.getJsonArray(JSON_SLIDES);
        for (int i = 0; i < jsonSlidesArray.size(); i++) {
	    JsonObject slideJso = jsonSlidesArray.getJsonObject(i);
	    slideShowToLoad.addSlide(	slideJso.getString(JSON_IMAGE_FILE_NAME),
					slideJso.getString(JSON_IMAGE_PATH), slideJso.getString(JSON_CAPTION));
	}
       newFile = false;
    }
    // AND HERE ARE THE PRIVATE HELPER METHODS TO HELP THE PUBLIC ONES
    
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }    
    
    private ArrayList<String> loadArrayFromJSONFile(String jsonFilePath, String arrayName) throws IOException {
        JsonObject json = loadJSONFile(jsonFilePath);
        ArrayList<String> items = new ArrayList();
        JsonArray jsonArray = json.getJsonArray(arrayName);
        for (JsonValue jsV : jsonArray) {
            items.add(jsV.toString());
        }
        return items;
    }
    
    private JsonArray makeSlidesJsonArray(List<Slide> slides) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Slide slide : slides) {
	    JsonObject jso = makeSlideJsonObject(slide);
	    jsb.add(jso);
        }
        JsonArray jA = jsb.build();
        return jA;        
    }
    
    private JsonArray makeCaptionJsonArray(List<String> caption){
        JsonArrayBuilder jsb2 = Json.createArrayBuilder();
        for(int i = 0; i < caption.size(); i++){
            JsonObject jso2 = makeCaptionJsonObject(caption.get(i));
            jsb2.add(jso2);
        }
        JsonArray jA2 = jsb2.build();
        return jA2;
    }
    
    private JsonObject makeSlideJsonObject(Slide slide) {
        JsonObject jso = Json.createObjectBuilder()
		.add(JSON_IMAGE_FILE_NAME, slide.getImageFileName())
		.add(JSON_IMAGE_PATH, slide.getImagePath())
              .add(JSON_CAPTION, slide.getCaption())
		.build();
	return jso;
    }
    
    private JsonObject makeCaptionJsonObject(String caption){
    
        JsonObject jso2 = Json.createObjectBuilder()
                .add(JSON_CAPTION, caption)
                .build();
        return jso2;
    }
}
