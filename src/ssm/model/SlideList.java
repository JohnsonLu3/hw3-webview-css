/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssm.model;

import javafx.collections.ObservableList;

/**
 *
 * @author JL
 */
public class SlideList {
    
    public static ObservableList<Slide> slideList;
    
    public static void setList(ObservableList<Slide> list){
        slideList = list;
    }
    
    public static ObservableList getList(){
        return slideList;
    }
    
    
}
