package ssm.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import ssm.view.SelectedSlide;
import ssm.view.SlideEditView;
import ssm.view.SlideShowMakerView;

/**
 * This class manages all the data associated with a slideshow.
 *
 * @author McKilla Gorilla & _____________
 */
public class SlideShowModel {

    SlideShowMakerView ui;
    String title;
    ObservableList<Slide> slides;
    Slide selectedSlide;
    String caption;


    public SlideShowModel(SlideShowMakerView initUI) {
        ui = initUI;
        slides = FXCollections.observableArrayList();
        reset();
    }

    // ACCESSOR METHODS
    public boolean isSlideSelected() {
        return selectedSlide != null;
    }

    public ObservableList<Slide> getSlides() {
        return slides;
    }

    public Slide getSelectedSlide() {
        return selectedSlide;
    }

    public String getTitle() {
        return title;
    }
    


    // MUTATOR METHODS
    public void setSelectedSlide(Slide initSelectedSlide) {
        selectedSlide = initSelectedSlide;
    }

    public void setTitle(String initTitle) {
        title = initTitle;
    }
    
    public void remove(){
        setSelectedSlide(SelectedSlide.getSelectedSlide());
        slides.remove(selectedSlide);
        SelectedSlide.deselectSlide();
        ui.reloadSlideShowPane(this);
    }
    
    public String getCaption(){
        return SlideEditView.getSlideCaption();
    }


    // SERVICE METHODS
    /**
     * Resets the slide show to have no slides and a default title.
     */
    public void reset() {
        slides.clear();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        title = props.getProperty(LanguagePropertyType.DEFAULT_SLIDE_SHOW_TITLE);
        selectedSlide = null;

    }

    /**
     * Adds a slide to the slide show with the parameter settings.
     *
     * @param initImageFileName File name of the slide image to add.
     * @param initImagePath File path for the slide image to add.
     */
    public void addSlide(String initImageFileName,
            String initImagePath, String initCaption) {
        Slide slideToAdd = new Slide(initImageFileName, initImagePath, initCaption);
        slides.add(slideToAdd);
        System.out.println(slides.size());
        selectedSlide = slideToAdd;
        ui.reloadSlideShowPane(this);
        SlideList.setList(slides);
    }
    
    
    public void moveSlideUp(){
        setSelectedSlide(SelectedSlide.getSelectedSlide());
        int index = slides.indexOf(getSelectedSlide());

            initTempSlide(slides.get((index - 1)));
            Slide tempSlide = new Slide( getTempFN(),getTempFP(), getTempC());
            slides.get((index-1)).setAll(SelectedSlide.getSelectedSlide());
            slides.get((index)).setAll(tempSlide);
            ui.reloadSlideShowPane(this);
            SelectedSlide.deselectSlide();
    }
    
    public void moveSlideDown(){
        setSelectedSlide(SelectedSlide.getSelectedSlide());
        int index = slides.indexOf(getSelectedSlide());

            initTempSlide(slides.get((index + 1)));
            Slide tempSlide = new Slide( getTempFN(),getTempFP(), getTempC());
            slides.get((index+1)).setAll(SelectedSlide.getSelectedSlide());
            slides.get((index)).setAll(tempSlide);
            ui.reloadSlideShowPane(this);
            SelectedSlide.deselectSlide();
    }
    
    private String tempFileName;
    private String tempPath;
    private String tempImageCaption;
    
    public void initTempSlide(Slide tempslide){
        tempFileName = tempslide.getImageFileName();
        tempPath = tempslide.getImagePath();
        tempImageCaption = tempslide.getCaption();
    }
    
    public String getTempFN(){
        return tempFileName;
    }
    
    public String getTempFP(){
        return tempPath;
    }
    
    public String getTempC(){
        return tempImageCaption;
    }


}
