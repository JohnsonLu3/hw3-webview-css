package ssm.controller;

import java.io.File;
import java.io.IOException;
import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DialogPane;
import javafx.stage.FileChooser;
import properties_manager.PropertiesManager;
import static ssm.LanguagePropertyType.ALERT;
import static ssm.LanguagePropertyType.CANCEL;
import static ssm.LanguagePropertyType.ERROR_TITLE;
import static ssm.LanguagePropertyType.ERROR_TXT;
import static ssm.LanguagePropertyType.FILE_NOT_FOUND;
import static ssm.LanguagePropertyType.FILE_NOT_FOUND_TITLE;
import static ssm.LanguagePropertyType.NEW_SLIDESHOW;
import static ssm.LanguagePropertyType.NO;
import static ssm.LanguagePropertyType.SAVE;
import static ssm.LanguagePropertyType.SAVE_MESS;
import static ssm.LanguagePropertyType.YES;
import static ssm.StartupConstants.PATH_CSS;
import ssm.model.SlideShowModel;
import ssm.error.ErrorHandler;
import ssm.file.SlideShowFileManager;
import ssm.view.SlideShowMakerView;
import static ssm.StartupConstants.PATH_SLIDE_SHOWS;
/**
 * This class serves as the controller for all file toolbar operations,
 * driving the loading and saving of slide shows, among other things.
 * 
 * @author McKilla Gorilla & _____________
 */
public class FileController {

    // WE WANT TO KEEP TRACK OF WHEN SOMETHING HAS NOT BEEN SAVED
    private boolean saved;

    // THE APP UI
    private SlideShowMakerView ui;
    
    // THIS GUY KNOWS HOW TO READ AND WRITE SLIDE SHOW DATA
    private SlideShowFileManager slideShowIO;

    /**
     * This default constructor starts the program without a slide show file being
     * edited.
     *
     * @param initSlideShowIO The object that will be reading and writing slide show
     * data.
     */
    public FileController(SlideShowMakerView initUI, SlideShowFileManager initSlideShowIO) {
        // NOTHING YET
        saved = true;
	ui = initUI;
        slideShowIO = initSlideShowIO;
    }
    
    public void markAsEdited() {
        saved = false;
        System.out.println("EDITED: " + !saved);
        ui.updateToolbarControls(saved);
    }

    /**
     * This method starts the process of editing a new slide show. If a pose is
     * already being edited, it will prompt the user to save it first.
     */
    public void handleNewSlideShowRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToMakeNew = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToMakeNew = promptToSave();
            }

            // IF THE USER REALLY WANTS TO MAKE A NEW COURSE
            if (continueToMakeNew) {
                // RESET THE DATA, WHICH SHOULD TRIGGER A RESET OF THE UI
                SlideShowModel slideShow = ui.getSlideShow();
                slideShow.reset();
                saved = false;

                // REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
                // THE APPROPRIATE CONTROLS
                ui.updateToolbarControls(saved);

                // TELL THE USER THE SLIDE SHOW HAS BEEN CREATED
                PropertiesManager props = PropertiesManager.getPropertiesManager();
                String NEWSLIDE = props.getProperty(NEW_SLIDESHOW.toString());
                String AMESS = props.getProperty(ALERT.toString());
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle(AMESS);
                alert.setHeaderText(null);
                alert.setContentText(NEWSLIDE);
                String alertCSS = "file:" + PATH_CSS + "alert.css";
                DialogPane alertStyle = alert.getDialogPane();
                alertStyle.getStylesheets().add(alertCSS);
                alertStyle.getStyleClass().add("alertStyle");

                alert.showAndWait();
                slideShowIO.setnewFile(true);
            }
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            // @todo provide error message
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String errorTitle = props.getProperty(ERROR_TITLE.toString());
            String error = props.getProperty(ERROR_TXT.toString());
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle(errorTitle);
            alert.setContentText(error);
            
            alert.showAndWait();
        }
    }

    /**
     * This method lets the user open a slideshow saved to a file. It will also
     * make sure data for the current slideshow is not lost.
     */
    public void handleLoadSlideShowRequest(){
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToOpen = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToOpen = promptToSave();
            }

            // IF THE USER REALLY WANTS TO OPEN A POSE
            if (continueToOpen) {
                // GO AHEAD AND PROCEED MAKING A NEW POSE
                promptToOpen();
                saved = true;
            }
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            //@todo provide error message
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String file_404_TITLE = props.getProperty(ERROR_TITLE.toString());
            String file_404 = props.getProperty(ERROR_TXT.toString());
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle(file_404_TITLE);
            alert.setContentText(file_404);
            
            alert.showAndWait();
        }
    }

    /**
     * This method will save the current slideshow to a file. Note that we already
     * know the name of the file, so we won't need to prompt the user.
     */
    public boolean handleSaveSlideShowRequest() {
        try {
	    // GET THE SLIDE SHOW TO SAVE
	    SlideShowModel slideShowToSave = ui.getSlideShow();
	    
            // SAVE IT TO A FILE
            slideShowIO.saveSlideShow(slideShowToSave);

            // MARK IT AS SAVED
            saved = true;

            // AND REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
            // THE APPROPRIATE CONTROLS
            ui.updateToolbarControls(saved);
	    return true;
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            // @todo
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String FILE_404_TITLE = props.getProperty(FILE_NOT_FOUND_TITLE.toString());
            String FILE_404 = props.getProperty(FILE_NOT_FOUND.toString());
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle(FILE_404_TITLE);
            alert.setContentText(FILE_404);
            String alertCSS = "file:" + PATH_CSS + "alert.css";
            DialogPane alertStyle = alert.getDialogPane();
            alertStyle.getStylesheets().add(alertCSS);
            alertStyle.getStyleClass().add("alertStyle");

            alert.showAndWait();
	    return false;
        }
    }
    

     /**
     * This method will exit the application, making sure the user doesn't lose
     * any data first.
     */
    public void handleExitRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToExit = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE
                continueToExit = promptToSave();
            }

            // IF THE USER REALLY WANTS TO EXIT THE APP
            if (continueToExit) {
                // EXIT THE APPLICATION
                System.exit(0);
            }
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            // @todo
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String FILE_404_TITLE = props.getProperty(FILE_NOT_FOUND_TITLE.toString());
            String FILE_404 = props.getProperty(FILE_NOT_FOUND.toString());
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle(FILE_404_TITLE);
            alert.setContentText(FILE_404);
            String alertCSS = "file:" + PATH_CSS + "alert.css";
            DialogPane alertStyle = alert.getDialogPane();
            alertStyle.getStylesheets().add(alertCSS);
            alertStyle.getStyleClass().add("alertStyle");

            alert.showAndWait();
        }
    }

    /**
     * This helper method verifies that the user really wants to save their
     * unsaved work, which they might not want to do. Note that it could be used
     * in multiple contexts before doing other actions, like creating a new
     * pose, or opening another pose, or exiting. Note that the user will be
     * presented with 3 options: YES, NO, and CANCEL. YES means the user wants
     * to save their work and continue the other action (we return true to
     * denote this), NO means don't save the work but continue with the other
     * action (true is returned), CANCEL means don't save the work and don't
     * continue with the other action (false is retuned).
     *
     * @return true if the user presses the YES option to save, true if the user
     * presses the NO option to not save, false if the user presses the CANCEL
     * option to not continue.
     */
    private boolean promptToSave() throws IOException {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        boolean saveWork = true;
        /**
        if(newFile == false){*/
            Alert savePrompt = new Alert(AlertType.CONFIRMATION);
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String S_TITLE = props.getProperty(SAVE.toString());
            String S_MESS = props.getProperty(SAVE_MESS.toString());
            String YES_BUTT = props.getProperty(YES.toString());
            String NO_BUTT = props.getProperty(NO.toString());
            String CANCEL_BUTT = props.getProperty(CANCEL.toString());
            
            savePrompt.setTitle(S_TITLE);
            savePrompt.setHeaderText(S_MESS);

            ButtonType save = new ButtonType(YES_BUTT);
            ButtonType dontSave = new ButtonType(NO_BUTT);
            ButtonType cancel = new ButtonType(CANCEL_BUTT, ButtonData.CANCEL_CLOSE);

            savePrompt.getButtonTypes().setAll(save, dontSave, cancel);
            String alertCSS = "file:" + PATH_CSS + "alert.css";
            DialogPane alertStyle = savePrompt.getDialogPane();
            alertStyle.getStylesheets().add(alertCSS);
            alertStyle.getStyleClass().add("alertStyle");

            Optional<ButtonType> result = savePrompt.showAndWait();
                if (result.get() == save){
                    saveWork = true;
                } else if (result.get() == dontSave) {
                    saveWork = false;
                } else if (result.get() == cancel) {
                    saveWork = false;
                }

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (saveWork) {
            SlideShowModel slideShow = ui.getSlideShow();
            slideShowIO.saveSlideShow(slideShow);
            saved = true;
        } // IF THE USER SAID CANCEL, THEN WE'LL TELL WHOEVER
        // CALLED THIS THAT THE USER IS NOT INTERESTED ANYMORE
        else if (!true) {
            return false;
        }

        // IF THE USER SAID NO, WE JUST GO ON WITHOUT SAVING
        // BUT FOR BOTH YES AND NO WE DO WHATEVER THE USER
        // HAD IN MIND IN THE FIRST PLACE
        return true;
    }

    /**
     * This helper method asks the user for a file to open. The user-selected
     * file is then loaded and the GUI updated. Note that if the user cancels
     * the open process, nothing is done. If an error occurs loading the file, a
     * message is displayed, but nothing changes.
     */
    private void promptToOpen() throws IOException{
        // AND NOW ASK THE USER FOR THE COURSE TO OPEN
        FileChooser slideShowFileChooser = new FileChooser();
        slideShowFileChooser.setInitialDirectory(new File(PATH_SLIDE_SHOWS));
        File selectedFile = slideShowFileChooser.showOpenDialog(ui.getWindow());

        // ONLY OPEN A NEW FILE IF THE USER SAYS OK
        if (selectedFile != null) {
            try {
		SlideShowModel slideShowToLoad = ui.getSlideShow();
                slideShowIO.loadSlideShow(slideShowToLoad, selectedFile.getAbsolutePath());
                ui.reloadSlideShowPane(slideShowToLoad);
                saved = true;
                ui.updateToolbarControls(saved);
            } catch (Exception e) {
                ErrorHandler eH = ui.getErrorHandler();
                // @todo
                    PropertiesManager props = PropertiesManager.getPropertiesManager();
                    String file_404_TITLE = props.getProperty(ERROR_TITLE.toString());
                    String file_404 = props.getProperty(FILE_NOT_FOUND.toString());
                    Alert openError = new Alert(AlertType.ERROR);
                    openError.setTitle(file_404_TITLE);
                    openError.setContentText(file_404);
                    
                    String alertCSS = "file:" + PATH_CSS + "alert.css";
                    DialogPane alertStyle = openError.getDialogPane();
                    alertStyle.getStylesheets().add(alertCSS);
                    alertStyle.getStyleClass().add("alertStyle");

                    openError.showAndWait();
            }
        }
    }

    /**
     * This mutator method marks the file as not saved, which means that when
     * the user wants to do a file-type operation, we should prompt the user to
     * save current work first. Note that this method should be called any time
     * the pose is changed in some way.
     */
    public void markFileAsNotSaved() {
        saved = false;
    }

    /**
     * Accessor method for checking to see if the current pose has been saved
     * since it was last editing. If the current file matches the pose data,
     * we'll return true, otherwise false.
     *
     * @return true if the current pose is saved to the file, false otherwise.
     */
    public boolean isSaved() {
        return saved;
    }
}

