var capArray = ["Hello","Hello","Mac Text FIle","Hello Worlds"]; 
var imgArray = ["Images/BadlandsSouthDakota.jpg","Images/QuirponIslandNewfoundland.jpg","Images/ArchesUtah.jpg","Images/SignalHillNewfoundland.jpg"]; 
var arrayIndex = 0;
var play = false;

function setUpSlideShow(){
    
    var showTitle = document.getElementById("slideShowTitle");
    showTitle.innerHTML = "Mac Test file";
    var firstImg = document.getElementById("slideImg");
    firstImg.src ="Images/BadlandsSouthDakota.jpg";
    var firstCap = document.getElementById("slideCaption");
    firstCap.innerHTML = "Hello";
}

function lastImage(){

    console.log("MOVE BACK");
    var Image = document.getElementById("slideImg");
    var Caption = document.getElementById("slideCaption");
    
    if(arrayIndex == 0){
        Image.src=imgArray[imgArray.length-1];
        Caption.innerHTML = capArray[capArray.length-1];
        arrayIndex = imgArray.length-1;
    }else{
        Image.src=imgArray[arrayIndex-1];
        Caption.innerHTML = capArray[arrayIndex-1];
        arrayIndex = arrayIndex -1;
    }
}

function nextImage(){

    console.log("Move Foward");
    var Image = document.getElementById("slideImg");
    var Caption = document.getElementById("slideCaption");
    
    if(arrayIndex == (imgArray.length-1)){
        Image.src = imgArray[0];
        Caption.innerHTML = capArray[0];
        arrayIndex = 0;
    }else{
        Image.src = imgArray[arrayIndex+1];
        Caption.innerHTML = capArray[arrayIndex+1];
        arrayIndex = arrayIndex + 1
    }
}

var intervalID;

function startSlideShow(){
    play = !play;
    console.log(play);
    if(play == true){
        var playButt = document.getElementById("play");
        playButt.src="Images/stop.png";
        playSlideShow();
    }else{
        var playButt = document.getElementById("play");
        playButt.src="Images/play.png";
        clearInterval(intervalID);
    }

}

function playSlideShow(){

    intervalID = setInterval(function(){nextImage()}, 2000);
}