var capArray = ["1","2","3","4","5"]; 
var imgArray = ["Images/CarhengeNebraska.jpg","Images/BadlandsSouthDakota.jpg","Images/BryceCanyonUtah.jpg","Images/BryceCanyonUtah.jpg","Images/SignalHillNewfoundland.jpg"]; 
var arrayIndex = 0;
var play = false;

function setUpSlideShow(){
    
    var showTitle = document.getElementById("slideShowTitle");
    showTitle.innerHTML = "12345";
    var firstImg = document.getElementById("slideImg");
    firstImg.src ="Images/CarhengeNebraska.jpg";
    var firstCap = document.getElementById("slideCaption");
    firstCap.innerHTML = "1";
}

function lastImage(){

    console.log("MOVE BACK");
    var Image = document.getElementById("slideImg");
    var Caption = document.getElementById("slideCaption");
    
    if(arrayIndex == 0){
        Image.src=imgArray[imgArray.length-1];
        Caption.innerHTML = capArray[capArray.length-1];
        arrayIndex = imgArray.length-1;
    }else{
        Image.src=imgArray[arrayIndex-1];
        Caption.innerHTML = capArray[arrayIndex-1];
        arrayIndex = arrayIndex -1;
    }
}

function nextImage(){

    console.log("Move Foward");
    var Image = document.getElementById("slideImg");
    var Caption = document.getElementById("slideCaption");
    
    if(arrayIndex == (imgArray.length-1)){
        Image.src = imgArray[0];
        Caption.innerHTML = capArray[0];
        arrayIndex = 0;
    }else{
        Image.src = imgArray[arrayIndex+1];
        Caption.innerHTML = capArray[arrayIndex+1];
        arrayIndex = arrayIndex + 1
    }
}

var intervalID;

function startSlideShow(){
    play = !play;
    console.log(play);
    if(play == true){
        var playButt = document.getElementById("play");
        playButt.src="Images/stop.png";
        playSlideShow();
    }else{
        var playButt = document.getElementById("play");
        playButt.src="Images/play.png";
        clearInterval(intervalID);
    }

}

function playSlideShow(){

    intervalID = setInterval(function(){nextImage()}, 2000);
}